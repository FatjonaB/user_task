import Vue from 'vue'
import axios from 'axios'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'
import LiquorTree from 'liquor-tree'
import vuetify from './plugins/vuetify'
import '../src/assets/css/general.css'
import PortalVue from 'portal-vue'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'
import JsonExcel from 'vue-json-excel'
import * as VueGoogleMaps from 'vue2-google-maps'
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)
Vue.component('downloadExcel', JsonExcel)

Vue.use(LiquorTree)

Vue.use(PortalVue)

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCO4Chz0MKL08QDZu6OUNFsQEHJfYeydbs',
    libraries: 'places',
  },
})

Vue.mixin({
  methods: {
    intoNumber: function(input: any) {
      return parseFloat(input.toString()).toFixed(4)
    },
  },
})

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
axios.defaults.headers.common['Content-Type'] = 'application/json'
axios.defaults.headers.common['Accept'] = 'application/json'
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
axios.defaults.baseURL = process.env.VUE_APP_BASE_URL + ''

Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  render: h => h(App),
}).$mount('#app')
