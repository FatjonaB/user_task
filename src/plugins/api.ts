import axios from 'axios'

const urlString = window.location.href
export default (axiosOptions: any = {}, withoutAuthorization: any = false) => {
  const defaultOptions = {
    baseURL: process.env.VUE_APP_BASE_URL,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
    },
  }

  const options = { ...defaultOptions, ...axiosOptions }
  return axios.create(options)
}
