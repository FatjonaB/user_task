# app

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

Inside **src/components** will be stored all components that will be used (re-usable components) in the project, like:buttons,selects,toast etc.
Inside **src/views** will be stored components(pages) we want the user to navigate, which are typically located at **src/router/routes.js**.
Every component inside **views** will have this structure:components, views.
At **components** will be stored components that will be used inside the components we are working on.
At **views** will be stored components we want the user to navigate. (as mentioned above)
